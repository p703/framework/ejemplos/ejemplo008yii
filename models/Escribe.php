<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "escribe".
 *
 * @property int $autor
 * @property int $libro
 *
 * @property Autores $autor0
 * @property Libros $libro0
 */
class Escribe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'escribe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autor', 'libro'], 'required'],
            [['autor', 'libro'], 'integer'],
            [['libro'], 'unique'],
            [['autor', 'libro'], 'unique', 'targetAttribute' => ['autor', 'libro']],
            [['autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['autor' => 'id']],
            [['libro'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::className(), 'targetAttribute' => ['libro' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'autor' => 'Autor',
            'libro' => 'Libro',
        ];
    }

    /**
     * Gets query for [[Autor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutor0()
    {
        return $this->hasOne(Autores::className(), ['id' => 'autor']);
    }

    /**
     * Gets query for [[Libro0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLibro0()
    {
        return $this->hasOne(Libros::className(), ['id' => 'libro']);
    }
}
