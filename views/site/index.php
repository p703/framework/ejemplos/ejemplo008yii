<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Gestión Libros';
?>
<div class="site-index">
    <div class="jumbotron text-center bg-transparent">
    <h1 class="display-4">Ejemplo de Aplicación</h1>
    
    <p class="lead">Gestión de Libros y Autores</p>
    
    </div>
    <div class="body-content">

        <div class="row">
            <div class="col-lg-10 mx-auto">
                <?= Html::img('@web/imgs/1.png', ['alt' => 'Diseño','class'=>'img-fluid']) ?>
            </div>
            
        </div>

    </div>
</div>
